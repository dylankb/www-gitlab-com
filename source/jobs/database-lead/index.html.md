---
layout: job_page
title: "Database Lead"
---

The Database Lead manages the team of [Database Specialists](/jobs/specialist/database/)
and reports to the [Director of Infrastructure](/jobs/director-of-infrastructure/).
The [Database Team](/handbook/infrastructure/database/) is responsible for
ensuring high levels of data availability and integrity while keeping the
database (and the application's use of the database) efficient, fast, and scalable.

## Responsibilities

- Drive initiatives around "leveling up" GitLab.com's database availability,
reliability, and scalability.
- Set policies and procedures for disaster recovery and data archiving.
- Manage a team of Database Specialists, including coaching and performance management and reviews.
- Plan and execute on the team's growth.
- Set and track team goals from the weekly, quarterly, to annual timescale.
- Introduce best practices and training for Developers on topics related to Database
querying and functionality.
- This role does include on-call rotations to respond to
GitLab.com availability incidents. More information on how our on-call works can
be found in our
[handbook](https://about.gitlab.com/handbook/on-call/#production-team-on-call-rotation).

## Requirements

- Experience in evaluating and implementing various disaster recovery mechanisms
- At least intermediate abilities in Ruby (preferred), Python, or other dynamic languages.
- Experience using frameworks such as Ruby on Rails, Django, Hibernate.
- Solid understanding of the internals of PostgreSQL.
- Experience _scaling_ PostgreSQL in a large production environment.
- Experience with Databases in a cloud environment.
- Management experience (3+ years).
- You share our [values](/handbook/#values), and work in accordance with those values.
- Excellent written and verbal English communication skills.
